import express from 'express';
import Counter from './counter.mjs';
import logger from './logger.mjs';
import endpoint_mw from './endpoint.mw.mjs';

const app = express();
const counter = new Counter();

// Middleware
app.use(endpoint_mw);

// Endpoints
app.get('', (req, res) => res.send("Welcome!"));
// GET /counter-increase
app.get('/counter-increase', (req, res) => {
  counter.increase();
  res.send(`${counter.read()}`); // Need to make for example to string so does not look like status code
});
// GET /counter-read
app.get('/counter-read', (req, res) => {
  //logger.http("[ENDPOINT] GET /counter-read");
  //logger.log("http", "[ENDPOINT] GET /counter-read"); // Same as above
  //logger.info("[ENDPOINT] GET /counter-read");
  // const url = req.url; // <- Mikä tuo on
  // const method = req.method; // <- voiko hyödyntää?
  res.send(`${counter.read()}`);
});
// GET /counter-zero
app.get('/counter-zero', (req, res) => {
  counter.zero();
  
  res.send(`${counter.read()}`);
});

app.all('*', (req, res) => {
  res.status(404).send("Resource not found.")
});

export default app;