import logger from './logger.mjs';


/** @type {import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
  // TODO...
  logger.info(`[ENDPOINT] ${req.method} '${req.url}'`);
  //console.log('middleware working!');// temporary
  next();
};

export default endpoint_mw;