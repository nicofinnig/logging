class Counter {
  constructor(initialCount = 0) {
    this.count = initialCount;
  }

  increase() {
    this.count++;
  }

  zero() {
    this.count = 0;
  }

  read() {
    return this.count;
  }
}

export default Counter;